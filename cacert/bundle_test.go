package cacert

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBundleWrite_EmptyContent(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	require.NoError(t, err, "unable to create test dir tree")
	defer os.RemoveAll(tmpDir)

	// import bundle
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	content := ""

	err = bundle{content: content, path: path}.write()
	require.NoError(t, err)

	// check that import path is not a file
	_, err = os.Stat(path)
	if !os.IsNotExist(err) {
		if err == nil {
			t.Errorf("Expected %s to NOT exist when additional ca cert bundle is an empty string", path)
		} else {
			t.Fatalf("Unexpected error: %v", err)
		}
	}
}

func TestBundleWrite_EmptyPath(t *testing.T) {
	err := bundle{content: "xyz", path: ""}.write()
	require.NoError(t, err)
}

func TestBundleWrite_ExistingImportDir(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	require.NoError(t, err, "unable to create test dir tree")
	defer os.RemoveAll(tmpDir)

	// import bundle
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	require.NoError(t, err)

	expectedContent := fmt.Sprintf("%s\n", content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_MissingImportDir(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	require.NoError(t, err, "unable to create test dir tree")
	defer os.RemoveAll(tmpDir)

	// import bundle
	path := filepath.Join(tmpDir, "does", "not", "exist", "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	require.NoError(t, err)

	expectedContent := fmt.Sprintf("%s\n", content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_ExistingFile(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	require.NoError(t, err, "unable to create test dir tree")
	defer os.RemoveAll(tmpDir)

	// create file prior to the import
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	originalValue := "some original value\n"
	err = ioutil.WriteFile(path, []byte(originalValue), 0644)
	require.NoError(t, err)

	// import bundle
	content := "Some Bundle!"
	err = bundle{content: content, path: path}.write()
	require.NoError(t, err)

	expectedContent := fmt.Sprintf("%s%s\n", originalValue, content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func TestBundleWrite_Twice(t *testing.T) {
	// prepare directory for import path
	tmpDir, err := ioutil.TempDir("", "")
	require.NoError(t, err, "unable to create test dir tree")
	defer os.RemoveAll(tmpDir)

	// import bundle
	path := filepath.Join(tmpDir, "ca-bundle.crt")
	content := "Some Bundle!"

	err = bundle{content: content, path: path}.write()
	require.NoError(t, err)

	// write a duplicate entry
	err = bundle{content: content, path: path}.write()
	require.NoError(t, err)

	expectedContent := fmt.Sprintf("%s\n%s\n", content, content)
	// check imported bundle
	compareFile(t, path, expectedContent)
}

func compareFile(t *testing.T, path string, want string) {
	got, err := ioutil.ReadFile(path)
	require.NoError(t, err)
	require.Equal(t, want, string(got))
}

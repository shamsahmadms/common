package pathfilter

// ParseError occurs when a malformed exclude pattern
// is supplied on the path filter creation.
type ParseError struct {
	pattern string
}

func (e ParseError) Error() string {
	return "syntax error in excluded path: " + e.pattern
}

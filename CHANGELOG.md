# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The release dates can be found on the [releases page](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases).

## v3.2.2
### Changed

- Bump testify to 1.8.0 (!165)

## v3.2.1
### Changed

- Fix issues with `gotestsum` by not coloring newline characters in log messages (!163)

## v3.2.0
### Added

- Add support for double star glob patterns (!162)

## v3.1.0
### Added

- Add caller info for logging entries (!160)

## v3.0.1
### Fixed

- Fix all common/v2 paths to be common/v3 paths (!159)

## v3.0.0
### Changed

- Drops deprecated `command` and `issue` modules (!155)

## v2.24.1
### Fixed

- Fix git certificate error with `ADDITIONAL_CA_CERT_BUNDLE` by changing `DefaultBundlePath` (!154)

## v2.24.0
### Changed

- Add warning to `issue` and `command` modules to alert on maintenance mode (!152)

## v2.23.0
### Removed

- Removed `ruleset` package in favor of `analyzers/ruleset` (!141)

## v2.22.1
### Changed

- Fix CA Certificate bug by appending a newline when writing CA Certificate file (!140)

## v2.22.0
### Changed

- Bump urfave/cli to v2.3.0 (!135)

## v2.21.4
### Added

- Add missing patch and pre-release segments to report version (!134)

## v2.21.3
### Fixed

- Fixed a bug in the ruleset package that caused ruleset disablement not to be enforced (!136)

## v2.21.2
### Added

- Add debug logging for surfacing excluded findings based on path exclusions (!130)

## v2.21.1
### Fixed

- Fixed bug where null vulnerabilities could be reported (!131)

## v2.21.0
### Added

- Added ability to ignore vulnerabilities using rulesets (!129)

## v2.20.5
### Removed

- Drop unused issue.Mitigations field (!127)

## v2.20.4
### Added

- Added hackerone identifier type  (!125)

## v2.20.3
### Added

- Added default path for Secret Detection rulesets config (!127)

## v2.20.2
### Added

- Added `issue.raw_source_code_extract` to report (!126)

## v2.20.1
### Changed

- Warn if no files match instead of returning error (!122)

## v2.20.0
### Removed

- Remove `orchestrator` package, since Docker-in-Docker for SAST and Dependency Scanning are no longer supported (!120)

## v2.19.1
### Changed

- Enable feature availability enforcement for `ruleset` package (!118)

## v2.19.0
### Changed

- Updated golang dependencies to latest versions (!119)

## v2.18.0
### Added

- Added `location.crash_address`, `location.crash_type`, `location.crash_state`, `location.stacktrace_snippet` to report(!114)

## v2.17.0
### Added

- Add `ruleset` package (!115)

## v2.16.0
### Added

- Added `iid`,`dependency_path`, and `direct` to the dependency objects of the dependency list (!116)

## v2.15.0
### Added

- Added `scan.start_time`, `scan.end_time` and `scan.status` to report (!113)

## v2.14.0
### Changed

- Allow git to use the CA certificate bundle to verify peers when fetching/pushing via HTTPS  (!112)

## v2.13.0
### Changed

- Switch to the MIT Expat license (!104)

## v2.12.0
### Added

- Add `NewApp()` function for initializing cli (!108)

## v2.11.0
### Changed

- Automatically output `scan` object with `scanner` and `type` in report (!107)

## v2.10.4
### Added

- Added `scan` object with `scanner` and `type` to report (!105)

## v2.10.3
### Added

- Added info logs to the CLI commands (!101)

## v2.10.2
### Changed

- Changed logutil format to use RFC3339 for date time stamps, which is a profile of ISO 8601 (!100)

## v2.10.1
### Changed

- Use logrus instead of the log package (!96)
- Use logrus instead of the print functions of the fmt package (!96)

### Added

- Add `init()` to logutil that will set log level based on `SECURE_LOG_LEVEL` env var (!96)

## v2.10.0
### Changed
- Change `Location.Dependency` to a pointer, so that it's omitted in the JSON output when it's nil (!92)

### Added

- Add `commit` JSON field to vulnerability location (!92)
- Add `secret_detection` to the report categories (!92)

## v2.9.2

- Sort remediations by compare key (@cpanato) (!91)

## v2.9.1
### Changed

- make cacert.DefaultBundlePath public

## v2.9.0
## Added

- Add `id` JSON field to vulnerabilities (!79)

## v2.8.0
### Changed

- CA bundle writer to append to existing files (!86)
- cacert.Import function to accept cacert.ImportOptions to specify where to write the CA certificate bundle (!86)
- command.Config now accepts cacert.ImportOptions (!86)

## v2.7.0
### Added

- Common logrus format (!73)

## v2.6.2
### Removed

- Remove unused structs and types introduced for DAST (!75)

## v2.6.1
### Changed

- Refactor the Go package used to import CA cert bundles (!83)

## v2.6.0
### Added

- Custom CA Cert support via the `--additional-ca-cert-bundle` and the `ADDITIONAL_CA_CERT_BUNDLE` env var (!76)

## v2.5.7
### Removed

- Remove undefined level from confidence and severity

## v2.5.6
### Changed

- Suppress the progress message from Docker Registry on pulling analyzer image (!65)

## v2.5.5
### Added

- Log when downloading, starting analyzers (!64)

## v2.5.4
### Added

- Support for `Red Hat Security Advisory (RHSA)` and `Oracle Linux Security Data` vulnerability types (!62)

## v2.5.3
### Added

- New `VulnerabilitiesAsReport` option for orchestrators, to be set to `true` to generate report v1 (!60)

## v2.5.2
### Added

- `image` in `.vulnerabilities[].location` report node: for vulnerabilities found by Container Scanning (!53)
- `operating_system` in `.vulnerabilities[].location` report node: for vulnerabilities found by Container Scanning (!53)
- Bump format version from `2.2` to `2.3`

## v2.5.1
### Fixed

- Force uid/guid when copying project directory for `nodejs-scan`, fixes missing read permissions

## v2.5.0
### Added

- `site_area` in `vulnerabilities[].location` report node: for vulnerabilities found by DAST (!39)
- New identifier type `wasc` which represents [WASC-ID](http://projects.webappsec.org/Threat-Classification-Reference-Grid) (!39)
- New identifier type `zap_plugin_id` which represents [ZAProxy plugin ID](https://github.com/zaproxy/zaproxy/blob/master/docs/scanners.md) (!39)
- `scanned_urls` as a top-level report node: details for URLs that were successfully scanned by DAST tool (!39)
- `io_error_urls` as a top-level report node: details for URLs scanned by DAST tool for which the target server did not respond (!39)

## v2.4.2
### Fixed
- `DS_EXCLUDED_PATHS` not applied to dependency files (!41)

## v2.4.1
### Added

- Sort dependency files and dependencies when merging reports (!36)

## v2.4.0
### Added

- `dependency_files` added to the report syntax (!35)

## v2.3.0
### Added

- `SAST_EXCLUDED_PATHS` and `DS_EXCLUDED_PATHS` to filter reports
  and remove vulnerabilities matching a set of excluded paths/globs

## v2.2.0
### Added

- `SeverityLevel` and `ConfidenceLevel` structs instead of `Level`

### Changed

- "Ignore" Severity level is now encoded as "Info"
- "Critical" Confidence level is now encoded as "Confirmed"

### Removed

- `Level` struct
- "Experimental" level for Severity

## v2.1.6
### Added
- Analyze sub-command

## v2.1.5
### Fixed
- Correct default analyzer image tag from latest to 2

## v2.1.4
### Fixed
- Fix report sorting for run execution to ensure consistent order

## v2.1.3
### Added
- Sub-sort vulnerabilities using compare key

## v1.9.2
### Fixed
- Null vulnerabilities when report is empty

## v2.1.2
### Fixed
- Null vulnerabilities when report is empty

## v2.1.1
### Fixed
- Merging of remediations

## v2.1.0
### Added
- Remediations field

## v2.0.1
### Fixed
- Path of nested modules

## v2.0.0
### Added
- V2 of Go module
- Format version to reports

### Changed
- Update documentation

## v1.9.1
### Added
- Remove duplicate issues/vulnerabilities

## v1.9.0
### Added
- Dependency to location, generate compare key & message for DS

### Changed
- Improve comment about Severity and Confidence levels

## v1.8.1
### Added
- Remote checks flag

## v1.8.0
### Added
- Import orchestrator, table

## v1.7.0
### Added
- Dependency Scanning support
- Set Docker entrypoint, cmd
- Redirect to os.Stdout, os.Stderr

### Changed
- Switch to semantic versioning

### Fixed
- Godoc, no markup

### Removed
- Match func, unused
- Versioning information, not accurate

## v1.6.0
### Added
- Report format compliant with Data Model constraints
- Match plugin to template
- Notice about using GitLab EE issue tracker in the Analyzer template
- Notice about using GitLab EE issue tracker

### Changed
- Correctly describe LICENSE

### Removed
- app.Email from template

## v1.5.0
### Added
- JSON unmarshaller to Level type
- More properties to SAST report

## v1.4.0
### Added
- Sort by priority

## v1.3.0
### Added
- Plugins search.MatchFunc functions

## v1.2.0
### Changed
- Prefix env var with ANAYZER_

## v1.1.1
### Added
- Truncate existing artifact file before writing

## v1.1.0
### Added
- Analyze the root directory

### Removed
- Error wrapper

## v1.0.0
### Added
- License
- Readme
- GitLab CI config
- Run sub-command
- Convert sub-command
- Make search command return project dir
- Handler error when closing file
- Exit with code 1 when match function fails

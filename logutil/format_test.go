package logutil

import (
	"runtime"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

func Test_Format(t *testing.T) {
	logger := log.New()

	entry := log.NewEntry(logger)
	entry.Message = "log message goes here"
	entry.Level = log.InfoLevel

	timestamp, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
	entry.Time = timestamp

	t.Run("When the project value is set", func(t *testing.T) {
		f := Formatter{Project: "some project"}
		expected := "\x1b[0;32m[INFO] [some project] [2006-01-02T15:04:05Z] ▶ log message goes here\x1b[0m\n"

		logEntry, err := f.Format(entry)
		require.NoError(t, err)
		require.Equal(t, string(logEntry), expected)

	})

	t.Run("When the project value is not set", func(t *testing.T) {
		f := Formatter{}
		expected := "\x1b[0;32m[INFO] [2006-01-02T15:04:05Z] ▶ log message goes here\x1b[0m\n"

		logEntry, err := f.Format(entry)
		require.NoError(t, err)
		require.Equal(t, string(logEntry), expected)
	})

	t.Run("When logger is set to report the caller", func(t *testing.T) {
		entry.Caller = &runtime.Frame{
			File: "/app/main.go",
			Line: 100,
		}
		logger.SetReportCaller(true)

		f := Formatter{}
		expected := "\x1b[0;32m[INFO] [2006-01-02T15:04:05Z] [/app/main.go:100] ▶ log message goes here\x1b[0m\n"

		logEntry, err := f.Format(entry)
		require.NoError(t, err)
		require.Equal(t, string(logEntry), expected)
	})
}
